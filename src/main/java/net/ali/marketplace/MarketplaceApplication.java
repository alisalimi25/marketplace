package net.ali.marketplace;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootApplication
public class MarketplaceApplication implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(MarketplaceApplication.class, args);
    }

    private void printUsage() {
        System.out.println("java -jar marketplace.jar ADDRESS_FILE");
    }

    @Override
    public void run(String... args) throws Exception {
        if (args.length < 1) {
            printUsage();
            return;
        }

        // TODO: This bean creation can be done with Spring
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        UrlRetriever urlRetriever = new UrlRetrieverImpl(objectMapper);

        // TODO: This operation can be done in parallel
        try (Stream<String> stream = Files.lines(Paths.get(args[0]), Charset.forName("UTF-8"))) {
            stream.filter(line -> !line.isEmpty())
                    .map(urlRetriever::getContent)
                    .map(obj -> {
                        try {
                            return objectMapper.writeValueAsString(obj);
                        } catch (JsonProcessingException e) {
                            throw new RuntimeException(e);
                        }
                    })
                    .forEach(System.out::println);
        }
    }
}
