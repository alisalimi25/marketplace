package net.ali.marketplace;

import net.ali.marketplace.model.AddrGeolocation;

public interface UrlRetriever {
    AddrGeolocation getContent(String url); 
}
