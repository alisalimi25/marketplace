package net.ali.marketplace.model;

public class AddrGeolocation {
    public enum Status {
        FOUND, NOT_FOUND
    };

    private String address;
    private Status status;
    private Geolocation geolocation;

    public AddrGeolocation(String address, Status status, Geolocation geolocation) {
        super();
        this.address = address;
        this.status = status;
        this.geolocation = geolocation;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Geolocation getGeolocation() {
        return geolocation;
    }

    public void setGeolocation(Geolocation geolocation) {
        this.geolocation = geolocation;
    }

}
