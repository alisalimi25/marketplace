package net.ali.marketplace.model;

public class Geolocation {
	private double lat;
	private double lng;

	public Geolocation() {

	}

	public Geolocation(double lat, double lng) {
		super();
		this.lat = lat;
		this.lng = lng;
	}

	public double getLat() {
		return lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

}
