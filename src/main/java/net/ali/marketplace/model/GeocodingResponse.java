package net.ali.marketplace.model;

import java.util.List;

public class GeocodingResponse {
	public enum ResultType {
		OK, ZERO_RESULTS, OVER_QUERY_LIMIT, REQUEST_DENIED, INVALID_REQUEST, UNKNOWN_ERROR
	};

	private List<GeocodingAddress> results;
	private ResultType status;

	public List<GeocodingAddress> getResults() {
		return results;
	}

	public void setResults(List<GeocodingAddress> results) {
		this.results = results;
	}

	public ResultType getStatus() {
		return status;
	}

	public void setStatus(ResultType status) {
		this.status = status;
	}

}
