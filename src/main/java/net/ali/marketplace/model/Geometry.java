package net.ali.marketplace.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Geometry {
	@JsonProperty("location")
	private Geolocation location;

	public Geolocation getLocation() {
		return location;
	}

	public void setLocation(Geolocation location) {
		this.location = location;
	}

}
