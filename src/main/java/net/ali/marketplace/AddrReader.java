package net.ali.marketplace;

import java.util.List;

public interface AddrReader {
	List<String> readLines(int limit);
}
