package net.ali.marketplace;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.ali.marketplace.model.AddrGeolocation;
import net.ali.marketplace.model.GeocodingResponse;

public class UrlRetrieverImpl implements UrlRetriever {
    private static final Logger logger = LoggerFactory.getLogger(UrlRetrieverImpl.class);

    private final ObjectMapper objectMapper;
    private final String urlTemplate = "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyDgwEKal1-g42U2_UvOLlR3Upve5m-_qQE&address=%s";
//    private final String urlTemplate = "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyD2WijYaqFSa9skqpqXIyy1Sy2yiFdyX6s&address=%s";

    public UrlRetrieverImpl(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    private GeocodingResponse retrieve(URL api) {
        try (InputStream is = api.openConnection().getInputStream()) {
            return objectMapper.readValue(is, GeocodingResponse.class);
        } catch (IOException e) {
            return null;
        }
    }

    private boolean hasValidGeolocation(GeocodingResponse resp) {
        return resp.getStatus() == GeocodingResponse.ResultType.OK && resp.getResults() != null
                && !resp.getResults().isEmpty() && resp.getResults().get(0).getGeometry() != null;
    }

    private boolean noDataFound(GeocodingResponse resp) {
        if (resp.getStatus() == GeocodingResponse.ResultType.OVER_QUERY_LIMIT) {
            return false;
        }

        return resp.getStatus() == GeocodingResponse.ResultType.ZERO_RESULTS || resp.getResults().isEmpty();
    }

    private void backoff(int retry) {
        if (retry == 4) {
            return; // No backoff required
        }

        long sleepTime = ((long) Math.pow(2, retry)) * 1000;
        logger.debug("backoff for {} ms", sleepTime);

        try {
            Thread.sleep(sleepTime);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public AddrGeolocation getContent(String addr) {
        String addrEncoded;
        try {
            addrEncoded = URLEncoder.encode(addr, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }

        URL api = null;
        String url = String.format(urlTemplate, addrEncoded);
        try {
            api = new URL(url);
        } catch (MalformedURLException e) {
            logger.error("error", e);
            throw new IllegalArgumentException(e);
        }

        GeocodingResponse resp = null;
        for (int retry = 0; retry < 5; retry++) {
            resp = retrieve(api);
            if (resp != null) {
                if (hasValidGeolocation(resp)) {
                    return new AddrGeolocation(addr, AddrGeolocation.Status.FOUND,
                            resp.getResults().get(0).getGeometry().getLocation());
                }

                if (noDataFound(resp)) {
                    logger.debug("no results found, no point to retry");
                    return new AddrGeolocation(addr, AddrGeolocation.Status.NOT_FOUND, null);
                }
            }

            backoff(retry);
        }

        logger.debug("ran out of retries");
        return new AddrGeolocation(addr, AddrGeolocation.Status.NOT_FOUND, null);
    }
}
