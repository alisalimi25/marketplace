package net.ali.marketplace;

import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class UrlRetrieverTest {
	private UrlRetriever sut;

	@Before
	public void setUp() {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		sut = new UrlRetrieverImpl(objectMapper);
	}

	@Test
	public void testValidAddr() throws InterruptedException {
		sut.getContent("450 N mathilda ave. sunnyvale, ca");
	}
}
